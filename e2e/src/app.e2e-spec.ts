import { AppPage } from './app.po'
import { browser, logging } from 'protractor'

describe('App tests', () => {
  let page: AppPage

  beforeEach(() => {
    page = new AppPage()
  })

  it('should display Clash Stats as title', () => {
    page.navigateTo()
    expect(page.getTitleText()).toEqual('Clash Stats')
  })
})
