import { browser, by, element } from 'protractor'

export class LoginPage {
  navigateTo() {
    return browser.get('/login')
  }

  getEmailTextbox() {
    return element(by.css('input[formControlName=email]'))
  }
  getPasswordTextbox() {
    return element(by.css('input[formControlName=password]'))
  }

  getForm() {
    return element(by.css('form'))
  }

  getSubmitButton() {
    return element(by.buttonText('Login'))
  }
}
