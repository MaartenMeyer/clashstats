import { LoginPage } from './login.po'
import { browser } from 'protractor'

describe('Login tests', () => {
  let page: LoginPage

  beforeEach(() => {
    page = new LoginPage()
    page.navigateTo()
  })

  it('Login form should be valid', () => {
    page.getEmailTextbox().sendKeys('test@email.com')
    page.getPasswordTextbox().sendKeys('Password123')

    const form = page.getForm().getAttribute('class')
    expect(form).toContain('ng-valid')
  })

  it('Login form should be invalid', () => {
    page.getEmailTextbox().sendKeys('')
    page.getPasswordTextbox().sendKeys('')

    const form = page.getForm().getAttribute('class')
    expect(form).toContain('ng-invalid')
  })

  xit('Clicking login should set currentUser in localStorage', () => {
    page.getEmailTextbox().sendKeys('admin@gmail.com')
    page.getPasswordTextbox().sendKeys('Admin12345')

    page.getSubmitButton().click()

    const value = browser.executeScript(`return window.localStorage.getItem('currentUser');`)
    expect(value).toContain('admin@gmail.com')
  })
})
