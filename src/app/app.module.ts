import { BrowserModule } from '@angular/platform-browser'
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { AngularFontAwesomeModule } from 'angular-font-awesome'

import { AboutComponent } from './modules/about/about.component'
import { UsecaseComponent } from './modules/about/usecase/usecase.component'
import { DashboardComponent } from './modules/dashboard/dashboard.component'
import { EntitiesComponent } from './modules/about/entities/entities.component'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { ClansRoutingModule } from './modules/clans/clans-routing-module'
import { ClansModule } from './modules/clans/clans.module'
import { PlayersRoutingModule } from './modules/players/players-routing.module'
import { PlayersModule } from './modules/players/players.module'
import { AuthenticationRoutingModule } from './modules/authentication/authentication-routing'
import { AuthenticationModule } from './modules/authentication/authentication.module'
import { JwtInterceptor } from './core/helpers/jwt-interceptor'
import { ErrorInterceptor } from './core/helpers/error-interceptor'
import { PipesModule } from './modules/pipes/pipes.module'

@NgModule({
  declarations: [AppComponent, AboutComponent, UsecaseComponent, DashboardComponent, EntitiesComponent],
  imports: [
    AuthenticationModule,
    AuthenticationRoutingModule,
    AppRoutingModule,
    ClansModule,
    ClansRoutingModule,
    PlayersRoutingModule,
    PlayersModule,
    BrowserModule,
    RouterModule,
    NgbModule,
    FormsModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule,
    PipesModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
