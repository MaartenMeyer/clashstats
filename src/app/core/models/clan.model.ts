import { User } from '@app/core/models/user.model'
import { Player } from '@app/core/models/player.model'
import { Deserializable } from './deserializable.model'

export class Clan implements Deserializable {
  clanId: string
  name: string
  description: string
  creator: User
  image: string
  members?: Player[]

  deserialize(input: any): this {
    Object.assign(this, input)
    this.creator = new User().deserialize(input.creator)
    return this
  }
}
