import { Deserializable } from './deserializable.model'

export class User implements Deserializable {
  username: string
  email: string
  password: string
  token: string
  _id: string

  deserialize(input: any): this {
    Object.assign(this, input)
    return this
  }
}
