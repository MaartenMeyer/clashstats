import { Clan } from './clan.model'
import { User } from './user.model'
import { Deserializable } from './deserializable.model'
import { Base } from './base.model'

export class Player implements Deserializable {
  playerId: string
  name: string
  level: number
  creator: User
  image: string
  clan: Clan
  bases?: Base[]

  deserialize(input: any) {
    Object.assign(this, input)
    this.creator = new User().deserialize(input.creator)
    this.clan = new Clan().deserialize(input.clan)
    return this
  }
}
