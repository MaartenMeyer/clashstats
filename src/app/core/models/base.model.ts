import { User } from '@app/core/models/user.model'
import { Deserializable } from './deserializable.model'

export class Base implements Deserializable {
  _id: string
  title: string
  creator: User
  image: string
  link: string
  videos?: string[]

  deserialize(input: any): this {
    Object.assign(this, input)
    this.creator = new User().deserialize(input.creator)
    return this
  }
}
