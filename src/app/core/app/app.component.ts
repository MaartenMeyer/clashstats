import { Component, Input, OnInit } from '@angular/core'
import { Router, NavigationEnd } from '@angular/router'

import { AuthenticationService } from '../services/authentication.service'
import { User } from '../models/user.model'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isNavbarCollapsed = true
  title = 'Clash Stats'
  currentUser: User

  constructor(private router: Router, private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(user => (this.currentUser = user))
  }

  ngOnInit(): void {
    this.router.events.subscribe(evt => {
      if (!(evt instanceof NavigationEnd)) {
        return
      }
      window.scrollTo(0, 0)
    })
  }

  logout() {
    this.authenticationService.logout()
    this.router.navigate(['/login'])
  }
}
