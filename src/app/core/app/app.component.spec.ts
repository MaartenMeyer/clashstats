import { TestBed, async } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AppRoutingModule } from '@app/app-routing.module'

import { AppComponent } from './app.component'
import { AboutComponent } from '@app/modules/about/about.component'
import { EntitiesComponent } from '@app/modules/about/entities/entities.component'
import { PlayersComponent } from '@app/modules/players/players.component'
import { PlayerDetailComponent } from '@app/modules/players/player-detail/player-detail.component'
import { ClansComponent } from '@app/modules/clans/clans.component'
import { ClanDetailComponent } from '@app/modules/clans/clan-detail/clan-detail.component'
import { UsecaseComponent } from '@app/modules/about/usecase/usecase.component'
import { DashboardComponent } from '@app/modules/dashboard/dashboard.component'
import { LoginComponent } from '@app/modules/authentication/login/login.component'
import { RegisterComponent } from '@app/modules/authentication/register/register.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { AuthenticationService } from '../services/authentication.service'
import { UserService } from '../services/user.service'
import { ClanService } from '../services/clan.service'
import { PlayerService } from '../services/player.service'
import { PlayersListItemComponent } from '@app/modules/players/players-list-item/players-list-item.component'
import { ClansListItemComponent } from '@app/modules/clans/clans-list-item/clans-list-item.component'
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { ScrollingModule } from '@angular/cdk/scrolling'
import { ClanResolver } from '../services/resolvers/clan.resolver'
import { PlayerResolver } from '../services/resolvers/player.resolver'
import { PipesModule } from '@app/modules/pipes/pipes.module'

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgbModule,
        FormsModule,
        AppRoutingModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        ScrollingModule,
        PipesModule
      ],
      declarations: [
        AppComponent,
        DashboardComponent,
        AboutComponent,
        UsecaseComponent,
        EntitiesComponent,
        LoginComponent,
        RegisterComponent,
        PlayersComponent,
        PlayerDetailComponent,
        ClansComponent,
        ClanDetailComponent,
        PlayersListItemComponent,
        ClansListItemComponent
      ],
      providers: [
        AuthenticationService,
        UserService,
        ClanService,
        ClanResolver,
        PlayerService,
        PlayerResolver
      ]
    }).compileComponents()
  }))

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app).toBeTruthy()
  })

  it(`should have as title 'Clash Stats'`, () => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app.title).toEqual('Clash Stats')
  })
})
