import { TestBed } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { ClanService } from './clan.service'
import { Player } from '../models/player.model'
import { User } from '../models/user.model'
import { Clan } from '../models/clan.model'
import { environment } from '@environments/environment'

describe('ClanService', () => {
  let clanService: ClanService
  let httpMock: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ClanService]
    })
    clanService = TestBed.get(ClanService)
    httpMock = TestBed.get(HttpTestingController)
  })

  afterEach(() => {
    httpMock.verify()
  })

  it('should be created', () => {
    expect(clanService).toBeTruthy()
  })

  describe('clans', () => {
    const user = new User()
    user._id = 'user'

    const player1: Player = {
      playerId: 'player-1',
      name: 'Player 1',
      level: 100,
      creator: user,
      clan: null,
      bases: null,
      image: 'image',
      deserialize: null
    }
    const player2: Player = {
      playerId: 'player-2',
      name: 'Player 2',
      level: 100,
      creator: user,
      clan: null,
      bases: null,
      image: 'image',
      deserialize: null
    }
    const players: Player[] = [player1, player2]

    const clan1: Clan = {
      clanId: 'clan-1',
      name: 'Clan 1',
      description: 'Clan 1 description',
      creator: user,
      image: 'image',
      members: players,
      deserialize: null
    }

    const clan2: Clan = {
      clanId: 'clan-2',
      name: 'Clan 2',
      description: 'Clan 2 description',
      creator: user,
      image: 'image',
      members: players,
      deserialize: null
    }

    it('createClan() should create clan', () => {
      const image: File = null
      clanService.createClan(clan1.clanId, clan1.name, clan1.description, image).subscribe(data => {
        expect(data.clanId).toEqual(clan1.clanId)
        expect(data.name).toEqual(clan1.name)
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/clans`)
      expect(req.request.method).toBe('POST')
      req.flush(clan1)
    })

    it('getAllClans() should return clans', () => {
      const clans: Clan[] = [clan1, clan2]
      clanService.getAllClans().subscribe(data => {
        expect(data.length).toBe(2)
        expect(data[0].name).toEqual(clan1.name)
        expect(data[1].name).toEqual(clan2.name)
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/clans`)
      expect(req.request.method).toBe('GET')
      req.flush(clans)
    })

    it('getClanById() should return clan', () => {
      clanService.getClanById(clan1.clanId).subscribe(data => {
        expect(data.name).toEqual(clan1.name)
        expect(data.clanId).toEqual(clan1.clanId)
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/clans/${clan1.clanId}`)
      expect(req.request.method).toBe('GET')
      req.flush(clan1)
    })

    it('updateClanById() should update clan', () => {
      clanService.updateClanById(clan1.clanId, 'New Description').subscribe((data: any) => {
        expect(data.message).toBe(`Clan ${clan1.clanId} updated`)
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/clans/${clan1.clanId}`)
      expect(req.request.method).toBe('PATCH')
      req.flush({
        message: `Clan ${clan1.clanId} updated`
      })
    })

    it('deleteClanById() should delete clan', () => {
      clanService.deleteClanById(clan1.clanId).subscribe((data: any) => {
        expect(data.message).toBe(`Clan ${clan1.clanId} deleted`)
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/clans/${clan1.clanId}`)
      expect(req.request.method).toBe('DELETE')
      req.flush({
        message: `Clan ${clan1.clanId} deleted`
      })
    })

    it('addPlayerToClan() should add player to clan', () => {
      clanService.addPlayerToClan(clan1.clanId, player1.playerId).subscribe((data: any) => {
        expect(data.message).toBe(`Player ${player1.playerId} added to clan ${clan1.clanId}`)
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/clans/${clan1.clanId}/players`)
      expect(req.request.method).toBe('PUT')
      req.flush({
        message: `Player ${player1.playerId} added to clan ${clan1.clanId}`
      })
    })

    it('removePlayerFromClan() should remove player from clan', () => {
      clanService.removePlayerFromClan(clan1.clanId, player1.playerId).subscribe((data: any) => {
        expect(data.message).toBe(`Player ${player1.playerId} removed from clan ${clan1.clanId}`)
      })
      const req = httpMock.expectOne(
        `${environment.baseUrl}/clans/${clan1.clanId}/players/${player1.playerId}`
      )
      expect(req.request.method).toBe('DELETE')
      req.flush({
        message: `Player ${player1.playerId} removed from clan ${clan1.clanId}`
      })
    })
  })
})
