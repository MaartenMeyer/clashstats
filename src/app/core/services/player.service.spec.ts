import { TestBed } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { PlayerService } from './player.service'
import { Player } from '../models/player.model'
import { User } from '../models/user.model'
import { Clan } from '../models/clan.model'
import { Base } from '../models/base.model'
import { environment } from '@environments/environment'

describe('PlayerService', () => {
  let playerService: PlayerService
  let httpMock: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PlayerService]
    })
    playerService = TestBed.get(PlayerService)
    httpMock = TestBed.get(HttpTestingController)
  })

  afterEach(() => {
    httpMock.verify()
  })

  it('should be created', () => {
    expect(playerService).toBeTruthy()
  })

  describe('createPlayer()', () => {
    const user = new User()
    user._id = 'user'

    const clan = new Clan()
    const base: Base = {
      _id: '123',
      title: 'Test Base',
      creator: user,
      image: 'image',
      link: 'link',
      deserialize: null
    }
    const bases: Base[] = [base]

    const player1: Player = {
      playerId: '12345',
      name: 'Player 1',
      level: 100,
      creator: user,
      clan,
      bases,
      image: 'image',
      deserialize: null
    }
    const player2: Player = {
      playerId: '54321',
      name: 'Player 2',
      level: 100,
      creator: user,
      clan,
      bases,
      image: 'image',
      deserialize: null
    }

    it('createPlayer() should create player', () => {
      const image: File = null
      playerService.createPlayer(player1.playerId, player1.name, player1.level, image).subscribe(player => {
        expect(player.name).toEqual(player1.name)
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/players`)
      expect(req.request.method).toBe('POST')
      req.flush(player1)
    })

    it('getAllPlayers() should return players', () => {
      const players: Player[] = [player1, player2]
      playerService.getAllPlayers().subscribe(data => {
        expect(data.length).toBe(2)
        expect(data[0].name).toEqual(player1.name)
        expect(data[1].name).toEqual(player2.name)
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/players`)
      expect(req.request.method).toBe('GET')
      req.flush(players)
    })

    it('getPlayerById() should return player', () => {
      playerService.getPlayerById(player1.playerId).subscribe(data => {
        expect(data.name).toEqual(player1.name)
        expect(data.playerId).toEqual(player1.playerId)
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/players/${player1.playerId}`)
      expect(req.request.method).toBe('GET')
      req.flush(player1)
    })

    it('getAllPlayersFromUser() should return players', () => {
      const players: Player[] = [player1, player2]
      playerService.getAllPlayersFromUser(user._id).subscribe(data => {
        expect(data.length).toBe(2)
        expect(data[0].name).toEqual(player1.name)
        expect(data[1].name).toEqual(player2.name)
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/players?userId=${user._id}`)
      expect(req.request.method).toBe('GET')
      req.flush(players)
    })

    it('updatePlayerById() should update player', () => {
      playerService.updatePlayerById(player1.playerId, 'New Name', 101).subscribe((data: any) => {
        expect(data.message).toBe('Player updated')
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/players/${player1.playerId}`)
      expect(req.request.method).toBe('PATCH')
      req.flush({
        message: 'Player updated'
      })
    })

    it('deletePlayerById() should delete player', () => {
      playerService.deletePlayerById(player1.playerId).subscribe((data: any) => {
        expect(data.message).toEqual('Player deleted')
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/players/${player1.playerId}`)
      expect(req.request.method).toBe('DELETE')
      req.flush({
        message: 'Player deleted'
      })
    })

    it('addBaseToPlayer() should add base to player', () => {
      const image: File = null
      playerService.addBaseToPlayer(player1.playerId, base.title, base.link, image).subscribe((data: any) => {
        expect(data.message).toBe(`Base added to player ${player1.playerId}`)
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/players/${player1.playerId}/bases`)
      expect(req.request.method).toBe('POST')
      req.flush({
        message: `Base added to player ${player1.playerId}`
      })
    })

    it('deleteBaseById() should delete base', () => {
      playerService.deleteBaseById(player1.playerId, base._id).subscribe((data: any) => {
        expect(data.message).toBe('Base deleted')
      })
      const req = httpMock.expectOne(`${environment.baseUrl}/players/${player1.playerId}/bases/${base._id}`)
      expect(req.request.method).toBe('DELETE')
      req.flush({
        message: 'Base deleted'
      })
    })
  })
})
