import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { environment } from '@environments/environment'
import { Clan } from '@app/core/models/clan.model'
import { of } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ClanService {
  constructor(private http: HttpClient) {}

  createClan(id: string, name: string, description: string, image: File) {
    const formData: any = new FormData()
    formData.append('clanId', id)
    formData.append('name', name)
    formData.append('description', description)
    formData.append('image', image)

    return this.http.post<Clan>(`${environment.baseUrl}/clans`, formData)
  }

  getAllClans() {
    return this.http.get<Clan[]>(`${environment.baseUrl}/clans`)
  }

  getClanById(id: string) {
    return this.http.get<Clan>(`${environment.baseUrl}/clans/${id}`)
  }

  updateClanById(id: string, description: string) {
    return this.http.patch(`${environment.baseUrl}/clans/${id}`, { description })
  }

  deleteClanById(id: string) {
    return this.http.delete(`${environment.baseUrl}/clans/${id}`)
  }

  addPlayerToClan(id: string, playerId: string) {
    return this.http.put(`${environment.baseUrl}/clans/${id}/players`, { playerId })
  }

  removePlayerFromClan(id: string, playerId: string) {
    return this.http.delete(`${environment.baseUrl}/clans/${id}/players/${playerId}`)
  }
}
