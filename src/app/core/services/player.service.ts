import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { environment } from '@environments/environment'
import { Player } from '@app/core/models/player.model'
import { Base } from '../models/base.model'

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  constructor(private http: HttpClient) {}

  createPlayer(id: string, name: string, level: number, image: File) {
    const formData: any = new FormData()
    formData.append('playerId', id)
    formData.append('name', name)
    formData.append('level', level)
    formData.append('image', image)

    return this.http.post<Player>(`${environment.baseUrl}/players`, formData)
  }

  getAllPlayers() {
    return this.http.get<Player[]>(`${environment.baseUrl}/players`)
  }

  getAllPlayersFromUser(id: string) {
    return this.http.get<Player[]>(`${environment.baseUrl}/players?userId=${id}`)
  }

  getPlayerById(id: string) {
    return this.http.get<Player>(`${environment.baseUrl}/players/${id}`)
  }

  updatePlayerById(id: string, name: string, level: number) {
    return this.http.patch(`${environment.baseUrl}/players/${id}`, { name, level })
  }

  deletePlayerById(id: string) {
    return this.http.delete(`${environment.baseUrl}/players/${id}`)
  }

  addBaseToPlayer(id: string, title: string, link: string, image: File) {
    const formData: any = new FormData()
    formData.append('title', title)
    formData.append('link', link)
    formData.append('image', image)

    return this.http.post<Base>(`${environment.baseUrl}/players/${id}/bases`, formData)
  }

  deleteBaseById(id: string, baseId: string) {
    return this.http.delete(`${environment.baseUrl}/players/${id}/bases/${baseId}`)
  }
}
