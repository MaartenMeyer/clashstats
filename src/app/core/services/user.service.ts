import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { environment } from '@environments/environment'
import { User } from '@app/core/models/user.model'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {}

  register(user: User) {
    return this.http.post(`${environment.baseUrl}/register`, user)
  }
}
