import { TestBed } from '@angular/core/testing'

import { ClanResolver } from './clan.resolver'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { ClanService } from '../clan.service'

describe('ClanResolver', () => {
  let clanResolver: ClanResolver

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [ClanResolver]
    })
    clanResolver = TestBed.get(ClanResolver)
  })

  it('should be created', () => {
    expect(clanResolver).toBeTruthy()
  })
})
