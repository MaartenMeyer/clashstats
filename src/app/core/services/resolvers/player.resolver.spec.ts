import { TestBed } from '@angular/core/testing'
import { Observable, of } from 'rxjs'

import { PlayerResolver } from './player.resolver'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { PlayerService } from '../player.service'
import { RouterTestingModule } from '@angular/router/testing'

describe('PlayerResolver', () => {
  let playerResolver: PlayerResolver

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [PlayerResolver]
    })
    playerResolver = TestBed.get(PlayerResolver)
  })

  it('should be created', () => {
    expect(playerResolver).toBeTruthy()
  })
})
