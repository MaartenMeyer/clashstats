import { Injectable } from '@angular/core'
import { map, first, catchError } from 'rxjs/operators'
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router'
import { Observable, of, empty } from 'rxjs'

import { ClanService } from '../clan.service'

@Injectable({
  providedIn: 'root'
})
export class ClanResolver implements Resolve<Observable<any>> {
  constructor(private clanService: ClanService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const id = route.paramMap.get('id')
    return this.clanService.getClanById(id).pipe(
      map(response => {
        if (response) {
          return response
        }
        this.router.navigate(['/clans'])
        return null
      }),
      catchError(error => {
        this.router.navigate(['/clans'])
        return of(null)
      })
    )
  }
}
