import { Injectable } from '@angular/core'
import { map, catchError } from 'rxjs/operators'
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router'
import { Observable, of } from 'rxjs'

import { PlayerService } from '../player.service'

@Injectable({
  providedIn: 'root'
})
export class PlayerResolver implements Resolve<Observable<any>> {
  constructor(private playerService: PlayerService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const id = route.paramMap.get('id')
    return this.playerService.getPlayerById(id).pipe(
      map(response => {
        if (response) {
          return response
        }
        this.router.navigate(['/players'])
        return null
      }),
      catchError(error => {
        this.router.navigate(['/players'])
        return of(null)
      })
    )
  }
}
