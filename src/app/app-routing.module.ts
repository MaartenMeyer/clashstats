import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { AboutComponent } from './modules/about/about.component'
import { DashboardComponent } from './modules/dashboard/dashboard.component'
import { AuthGuard } from './core/guards/auth.guard'

const appRoutes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  {
    path: 'login',
    loadChildren: () =>
      import('./modules/authentication/authentication.module').then(m => m.AuthenticationModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./modules/authentication/authentication.module')
  },
  { path: 'about', component: AboutComponent },
  {
    path: 'clans',
    loadChildren: () => import('./modules/clans/clans.module').then(m => m.ClansModule)
  },
  {
    path: 'players',
    loadChildren: () => import('./modules/players/players.module').then(m => m.PlayersModule)
  },
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
]

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
