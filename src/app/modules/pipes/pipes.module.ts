import { NgModule } from '@angular/core'
import { FilterPipe } from './pipes'

@NgModule({
  declarations: [FilterPipe],
  imports: [],
  exports: [FilterPipe]
})
export class PipesModule {}
