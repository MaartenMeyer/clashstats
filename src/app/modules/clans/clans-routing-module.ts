import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { ClansComponent } from './clans.component'
import { ClanDetailComponent } from './clan-detail/clan-detail.component'
import { ClanResolver } from '@app/core/services/resolvers/clan.resolver'
import { ClansOverviewComponent } from './clans-overview/clans-overview.component'

const clansRoutes: Routes = [
  {
    path: '',
    component: ClansComponent,
    children: [
      {
        path: '',
        redirectTo: 'overview',
        pathMatch: 'full'
      },
      {
        path: 'overview',
        component: ClansOverviewComponent
      },
      {
        path: ':id',
        children: [
          {
            path: '',
            redirectTo: 'details',
            pathMatch: 'full'
          },
          {
            path: 'details',
            component: ClanDetailComponent,
            resolve: { clan: ClanResolver },
            runGuardsAndResolvers: 'always'
          }
        ]
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(clansRoutes)],
  exports: [RouterModule],
  providers: [ClanResolver]
})
export class ClansRoutingModule {}
