import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ClansComponent } from './clans.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { ClanDetailComponent } from './clan-detail/clan-detail.component'
import { ClansListItemComponent } from './clans-list-item/clans-list-item.component'
import { ScrollingModule } from '@angular/cdk/scrolling'
import { PipesModule } from '../pipes/pipes.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

describe('ClansComponent', () => {
  let component: ClansComponent
  let fixture: ComponentFixture<ClansComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        ScrollingModule,
        PipesModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [ClansComponent, ClanDetailComponent, ClansListItemComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ClansComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
