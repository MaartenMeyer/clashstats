import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ClanDetailComponent } from './clan-detail.component'
import { RouterTestingModule } from '@angular/router/testing'
import { Clan } from '@app/core/models/clan.model'
import { User } from '@app/core/models/user.model'
import { Player } from '@app/core/models/player.model'
import { Observable, of } from 'rxjs'
import { ClanService } from '@app/core/services/clan.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { AuthenticationService } from '@app/core/services/authentication.service'
import { NO_ERRORS_SCHEMA } from '@angular/core'

describe('ClanDetailComponent', () => {
  let component: ClanDetailComponent
  let fixture: ComponentFixture<ClanDetailComponent>
  const user: User = {
    _id: 'user-1',
    username: 'test',
    email: 'test@email.com',
    password: 'Test12345',
    token: 'token',
    deserialize: null
  }
  const clan: Clan = {
    clanId: 'clan-1',
    name: 'Clan 1',
    description: 'Clan 1 description',
    creator: user,
    image: 'image',
    members: null,
    deserialize: null
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [ClanDetailComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [AuthenticationService, ClanService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ClanDetailComponent)
    component = fixture.componentInstance
    component.currentUser = user
    component.clan = clan
    fixture.detectChanges()
  })

  afterEach(() => {
    TestBed.resetTestingModule()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
