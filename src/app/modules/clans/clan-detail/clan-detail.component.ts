import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { first } from 'rxjs/operators'

import { ClanService } from '@app/core/services/clan.service'
import { AuthenticationService } from '@app/core/services/authentication.service'
import { Clan } from '@app/core/models/clan.model'
import { User } from '@app/core/models/user.model'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ClanJoinComponent } from '../modals/clan-join/clan-join.component'
import { ClanUpdateComponent } from '../modals/clan-update/clan-update.component'

@Component({
  selector: 'app-clan-detail',
  templateUrl: './clan-detail.component.html',
  styleUrls: ['./clan-detail.component.scss']
})
export class ClanDetailComponent implements OnInit {
  clan: Clan
  currentUser: User
  showError = false
  error = ''

  constructor(
    private clanService: ClanService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal
  ) {
    this.currentUser = this.authenticationService.currentUserValue
  }

  ngOnInit() {
    this.loadClan()
  }

  private reloadCurrentRoute() {
    const currentUrl = this.router.url
    this.router.navigate([currentUrl])
  }

  private loadClan() {
    this.route.data.subscribe(data => {
      this.clan = data.clan
    })
  }

  deleteClan() {
    if (confirm(`Are you sure you want to delete this clan?`)) {
      this.clanService
        .deleteClanById(this.clan.clanId)
        .pipe(first())
        .subscribe(
          response => {
            this.reloadCurrentRoute()
          },
          error => {
            this.error = error
            this.showError = true
            window.scrollTo(0, 0)
          }
        )
    }
  }

  removeMember(id) {
    if (confirm(`Remove player ${id} from clan?`)) {
      this.clanService
        .removePlayerFromClan(this.clan.clanId, id)
        .pipe(first())
        .subscribe(
          response => {
            this.reloadCurrentRoute()
          },
          error => {
            this.error = error
            this.showError = true
            window.scrollTo(0, 0)
          }
        )
    }
  }

  openUpdateModal() {
    const modalRef = this.modalService.open(ClanUpdateComponent)
    modalRef.componentInstance.clan = this.clan
    modalRef.componentInstance.currentUser = this.currentUser
    modalRef.result
      .then(result => {
        this.reloadCurrentRoute()
      })
      .catch(error => {
        this.error = error
        this.showError = true
        window.scrollTo(0, 0)
      })
  }

  openJoinModal() {
    const modalRef = this.modalService.open(ClanJoinComponent)
    modalRef.componentInstance.clan = this.clan
    modalRef.componentInstance.currentUser = this.currentUser
    modalRef.result
      .then(result => {
        this.reloadCurrentRoute()
      })
      .catch(error => {
        this.error = error
        this.showError = true
        window.scrollTo(0, 0)
      })
  }
}
