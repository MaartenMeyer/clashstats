import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ClanCreateComponent } from './clan-create.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { ClanService } from '@app/core/services/clan.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('ClanCreateComponent', () => {
  let component: ClanCreateComponent
  let fixture: ComponentFixture<ClanCreateComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, HttpClientTestingModule],
      declarations: [ClanCreateComponent],
      providers: [NgbActiveModal, ClanService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ClanCreateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
