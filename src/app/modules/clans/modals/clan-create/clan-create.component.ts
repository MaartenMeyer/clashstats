import { Component, OnInit, Input } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { first } from 'rxjs/operators'

import { ClanService } from '@app/core/services/clan.service'

@Component({
  selector: 'app-clan-create',
  templateUrl: './clan-create.component.html',
  styleUrls: ['./clan-create.component.scss']
})
export class ClanCreateComponent implements OnInit {
  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private clanService: ClanService
  ) {}

  @Input() id: number
  createClanForm: FormGroup
  showError = false
  error: ''
  submitted = false
  fileData: File = null
  previewUrl: any = null
  uploadedFilePath: string = null

  ngOnInit() {
    this.createClanForm = this.formBuilder.group({
      clanId: ['', [Validators.required]],
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      image: [null, [Validators.required]]
    })
  }

  get form() {
    return this.createClanForm.controls
  }

  readUrl(fileInput: any) {
    this.fileData = fileInput.target.files[0] as File
    this.preview()
  }

  preview() {
    const mimeType = this.fileData.type
    if (mimeType.match(/image\/*/) == null) {
      return
    }

    const reader = new FileReader()
    reader.readAsDataURL(this.fileData)
    reader.onload = event => {
      this.previewUrl = reader.result
    }
  }

  onSubmit() {
    this.submitted = true
    if (this.createClanForm.invalid) {
      return
    }

    this.clanService
      .createClan(
        this.createClanForm.value.clanId,
        this.createClanForm.value.name,
        this.createClanForm.value.description,
        this.fileData
      )
      .pipe(first())
      .subscribe(
        data => {
          this.activeModal.close('Clan created')
        },
        error => {
          this.showError = true
          this.error = error
        }
      )
  }
}
