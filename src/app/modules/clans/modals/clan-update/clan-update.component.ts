import { Component, OnInit, Input } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ClanService } from '@app/core/services/clan.service'
import { Clan } from '@app/core/models/clan.model'
import { User } from '@app/core/models/user.model'
import { first } from 'rxjs/operators'

@Component({
  selector: 'app-clan-update',
  templateUrl: './clan-update.component.html',
  styleUrls: ['./clan-update.component.scss']
})
export class ClanUpdateComponent implements OnInit {
  constructor(
    public activeModal: NgbActiveModal,
    public formBuilder: FormBuilder,
    private clanService: ClanService
  ) {}

  @Input() clan: Clan
  @Input() currentUser: User
  updateClanForm: FormGroup
  showError = false
  error: ''
  submitted = false

  ngOnInit() {
    this.updateClanForm = this.formBuilder.group({
      clanId: [this.clan.clanId, [Validators.required]],
      description: [this.clan.description, [Validators.required]]
    })
  }

  get form() {
    return this.updateClanForm.controls
  }

  onSubmit() {
    this.submitted = true
    if (this.updateClanForm.invalid) {
      return
    }

    this.clanService
      .updateClanById(this.updateClanForm.value.clanId, this.updateClanForm.value.description)
      .pipe(first())
      .subscribe(
        data => {
          this.activeModal.close('Clan updated')
        },
        error => {
          this.showError = true
          this.error = error
        }
      )
  }
}
