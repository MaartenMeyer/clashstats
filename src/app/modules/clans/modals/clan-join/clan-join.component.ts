import { Component, OnInit, Input } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ClanService } from '@app/core/services/clan.service'
import { PlayerService } from '@app/core/services/player.service'
import { Clan } from '@app/core/models/clan.model'
import { first } from 'rxjs/operators'
import { Player } from '@app/core/models/player.model'
import { User } from '@app/core/models/user.model'

@Component({
  selector: 'app-clan-join',
  templateUrl: './clan-join.component.html',
  styleUrls: ['./clan-join.component.scss']
})
export class ClanJoinComponent implements OnInit {
  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private clanService: ClanService,
    private playerService: PlayerService
  ) {}

  @Input() clan: Clan
  @Input() currentUser: User
  players: Player[] = []
  addToClanForm: FormGroup
  showError = false
  error: ''
  submitted = false

  ngOnInit() {
    this.addToClanForm = this.formBuilder.group({
      clanId: [this.clan.clanId, [Validators.required]],
      playerId: ['', [Validators.required]]
    })
    this.loadPlayers()
  }

  get form() {
    return this.addToClanForm.controls
  }

  loadPlayers() {
    this.playerService
      .getAllPlayersFromUser(this.currentUser._id)
      .pipe(first())
      .subscribe(players => {
        this.players = players
      })
  }

  onSubmit() {
    this.submitted = true
    if (this.addToClanForm.invalid) {
      return
    }

    this.clanService
      .addPlayerToClan(this.addToClanForm.value.clanId, this.addToClanForm.value.playerId)
      .pipe(first())
      .subscribe(
        data => {
          this.activeModal.close('Player added to clan')
        },
        error => {
          this.showError = true
          this.error = error
        }
      )
  }
}
