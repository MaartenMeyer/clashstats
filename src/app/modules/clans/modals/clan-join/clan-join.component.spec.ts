import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ClanJoinComponent } from './clan-join.component'
import { FormsModule, ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { ClanService } from '@app/core/services/clan.service'
import { PlayerService } from '@app/core/services/player.service'
import { Clan } from '@app/core/models/clan.model'
import { User } from '@app/core/models/user.model'

describe('ClanJoinComponent', () => {
  let component: ClanJoinComponent
  let fixture: ComponentFixture<ClanJoinComponent>
  const formBuilder: FormBuilder = new FormBuilder()
  const user: User = {
    _id: 'user-1',
    username: 'test',
    email: 'test@email.com',
    password: 'Test12345',
    token: 'token',
    deserialize: null
  }
  const clan: Clan = {
    clanId: 'clan-1',
    name: 'Clan 1',
    description: 'Clan 1 description',
    creator: user,
    image: 'image',
    members: null,
    deserialize: null
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, HttpClientTestingModule],
      declarations: [ClanJoinComponent],
      providers: [NgbActiveModal, ClanService, PlayerService, { provide: FormBuilder, useValue: formBuilder }]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ClanJoinComponent)
    component = fixture.componentInstance
    component.currentUser = user
    component.clan = clan
    component.addToClanForm = formBuilder.group({
      clanId: [clan.clanId, [Validators.required]],
      playerId: ['', [Validators.required]]
    })
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
