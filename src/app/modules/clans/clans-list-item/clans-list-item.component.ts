import { Component, OnInit, Input } from '@angular/core'
import { Clan } from '@app/core/models/clan.model'

@Component({
  selector: 'app-clans-list-item',
  templateUrl: './clans-list-item.component.html',
  styleUrls: ['./clans-list-item.component.scss']
})
export class ClansListItemComponent implements OnInit {
  @Input() clan: Clan

  constructor() {}

  ngOnInit() {}
}
