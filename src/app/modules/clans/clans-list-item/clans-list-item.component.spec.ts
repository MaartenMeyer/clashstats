import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ClansListItemComponent } from './clans-list-item.component'
import { RouterTestingModule } from '@angular/router/testing'

describe('ClansListItemComponent', () => {
  let component: ClansListItemComponent
  let fixture: ComponentFixture<ClansListItemComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClansListItemComponent],
      imports: [RouterTestingModule]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ClansListItemComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
