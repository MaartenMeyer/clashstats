import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { BrowserModule } from '@angular/platform-browser'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ScrollingModule } from '@angular/cdk/scrolling'

import { ClansRoutingModule } from './clans-routing-module'
import { ClansComponent } from './clans.component'
import { ClanDetailComponent } from './clan-detail/clan-detail.component'
import { ClansListItemComponent } from './clans-list-item/clans-list-item.component'
import { ClanCreateComponent } from './modals/clan-create/clan-create.component'
import { PipesModule } from '../pipes/pipes.module'
import { ClanJoinComponent } from './modals/clan-join/clan-join.component'
import { ClanUpdateComponent } from './modals/clan-update/clan-update.component'
import { ClansOverviewComponent } from './clans-overview/clans-overview.component'
@NgModule({
  imports: [
    ClansRoutingModule,
    CommonModule,
    ScrollingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule
  ],
  declarations: [
    ClansComponent,
    ClanDetailComponent,
    ClansListItemComponent,
    ClanCreateComponent,
    ClanJoinComponent,
    ClanUpdateComponent,
    ClansOverviewComponent
  ],
  entryComponents: [ClanCreateComponent, ClanJoinComponent, ClanUpdateComponent]
})
export class ClansModule {}
