import { Component, OnInit, Input } from '@angular/core'
import { Router } from '@angular/router'
import { first } from 'rxjs/operators'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'

import { ClanCreateComponent } from '../modals/clan-create/clan-create.component'
import { ClanService } from '@app/core/services/clan.service'
import { AuthenticationService } from '@app/core/services/authentication.service'
import { Clan } from '@app/core/models/clan.model'
import { User } from '@app/core/models/user.model'

@Component({
  selector: 'app-clans-overview',
  templateUrl: './clans-overview.component.html',
  styleUrls: ['./clans-overview.component.scss']
})
export class ClansOverviewComponent implements OnInit {
  @Input()
  clans: Clan[] = []
  currentUser: User
  searchText = ''
  showError = false
  error = ''
  loading = false

  constructor(
    private clanService: ClanService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.currentUser = this.authenticationService.currentUserValue
  }

  ngOnInit() {
    this.loading = true
    this.loadAllClans()
  }

  private reloadCurrentRoute() {
    const currentUrl = this.router.url
    this.router.navigateByUrl('../', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl])
    })
  }

  private loadAllClans() {
    this.clanService
      .getAllClans()
      .pipe(first())
      .subscribe(clans => {
        this.loading = false
        this.clans = clans
      })
  }

  openModal() {
    const modalRef = this.modalService.open(ClanCreateComponent)
    modalRef.componentInstance.id = 10
    modalRef.result
      .then(result => {
        this.reloadCurrentRoute()
      })
      .catch(error => {})
  }
}
