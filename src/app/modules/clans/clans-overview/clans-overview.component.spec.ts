import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ClansOverviewComponent } from './clans-overview.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { ClanDetailComponent } from '../clan-detail/clan-detail.component'
import { ClansListItemComponent } from '../clans-list-item/clans-list-item.component'
import { ScrollingModule } from '@angular/cdk/scrolling'
import { PipesModule } from '@app/modules/pipes/pipes.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

describe('ClansComponent', () => {
  let component: ClansOverviewComponent
  let fixture: ComponentFixture<ClansOverviewComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        ScrollingModule,
        PipesModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [ClansOverviewComponent, ClanDetailComponent, ClansListItemComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ClansOverviewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
