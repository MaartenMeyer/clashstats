import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AboutComponent } from './about.component'
import { UsecaseComponent } from './usecase/usecase.component'
import { EntitiesComponent } from './entities/entities.component'

describe('AboutComponent', () => {
  let component: AboutComponent
  let fixture: ComponentFixture<AboutComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AboutComponent, UsecaseComponent, EntitiesComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
