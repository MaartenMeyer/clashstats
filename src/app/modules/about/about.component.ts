import { Component, OnInit } from '@angular/core'
import { UseCase } from './usecase.model'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Een gebruiker kan met een emailadres en wachtwoord inloggen.',
      scenario: [
        'Gebruiker vult emailadres en wachtwoord in en klikt op Login knop.',
        'Applicatie valideert ingevoerde gegevens.',
        'Gegevens zijn correct: gebruiker wordt het dashboard getoond.'
      ],
      actor: 'Gebruiker',
      precondition: 'De gebruiker is nog niet ingelogd',
      postcondition: 'De gebruiker is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Uitloggen',
      description: 'Een gebruiker kan uitloggen.',
      scenario: [
        'Gebruiker klikt op uitloggen.',
        'Applicatie logt de gebruiker uit.',
        'Gebruiker wordt het dashboard getoond.'
      ],
      actor: 'Gebruiker',
      precondition: 'De gebruiker is ingelogd',
      postcondition: 'De gebruiker is uitgelogd'
    },
    {
      id: 'UC-03',
      name: 'Overzichtscherm Spelers openen',
      description: 'Een gebruiker kan een overzicht van alle spelers opvragen.',
      scenario: [
        'De gebruiker klikt in de navigatiebalk op Spelers.',
        'De applicatie toont het overzichtscherm Spelers.'
      ],
      actor: 'Gebruiker',
      precondition: '-',
      postcondition: 'Het overzicht van de spelers is aan de gebruiker getoond'
    },
    {
      id: 'UC-04',
      name: 'Speler zoeken',
      description: 'Een gebruiker kan in het overzichtscherm van spelers een speler zoeken.',
      scenario: [
        'De gebruiker typt iets in het inputveld.',
        'De applicatie zoekt naar overeenkomende Spelers en past de lijst aan.',
        'De applicatie toont de vernieuwde lijst aan de gebruiker.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-03 is uitgevoerd.',
      postcondition: 'De overeenkomende spelers zijn aan de gebruiker getoond.'
    },
    {
      id: 'UC-05',
      name: 'Detailscherm Speler openen',
      description: 'Een gebruiker kan de details van een speler opvragen.',
      scenario: ['De gebruiker klikt op een speler.', 'De applicatie toont het detailscherm Speler.'],
      actor: 'Gebruiker',
      precondition: 'UC-03 of UC-09 is uitgevoerd.',
      postcondition: 'De details van een speler zijn aan de gebruiker getoond'
    },
    {
      id: 'UC-06',
      name: 'Speler toevoegen',
      description: 'Een gebruiker kan een speler aanmaken en opslaan.',
      scenario: [
        'De gebruiker klikt op de Nieuwe Speler knop.',
        'De applicatie opent het Nieuwe Speler formulier',
        'De gebruiker vult de gegevens in en klikt op opslaan',
        'De applicatie slaat de speler op in de database.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-03 is uitgevoerd',
      postcondition: 'Er is een nieuwe spelers opgeslagen in de database'
    },
    {
      id: 'UC-07',
      name: 'Speler bewerken',
      description: 'Een gebruiker kan een speler bewerken en opslaan.',
      scenario: [
        'De gebruiker klikt op de Bewerken knop.',
        'De applicatie opent het Speler Bewerken formulier',
        'De gebruiker past de gegevens aan en klikt op opslaan',
        'De applicatie update de speler in de database.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-05 is uitgevoerd',
      postcondition: 'De gegevens van de speler zijn bewerkt en opgeslagen in de database'
    },
    {
      id: 'UC-08',
      name: 'Speler verwijderen',
      description: 'Een gebruiker kan een speler verwijderen.',
      scenario: [
        'De gebruiker klikt op de Verwijder knop.',
        'De applicatie verwijdert de speler uit de database.',
        'De applicatie toont de vernieuwde lijst aan de gebruiker.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-05 is uitgevoerd',
      postcondition: 'De speler is verwijderd uit de database'
    },
    {
      id: 'UC-09',
      name: 'Overzichtscherm Clans openen',
      description: 'Een gebruiker kan een overzicht van alle clans opvragen.',
      scenario: [
        'De gebruiker klikt in de navigatiebalk op Clans.',
        'De applicatie toont het overzichtscherm Clans.'
      ],
      actor: 'Gebruiker',
      precondition: '-',
      postcondition: 'Het overzicht van de clans is aan de gebruiker getoond.'
    },
    {
      id: 'UC-10',
      name: 'Clan zoeken',
      description: 'Een gebruiker kan in het overzichtscherm van clans een clan zoeken.',
      scenario: [
        'De gebruiker typt iets in het inputveld.',
        'De applicatie zoekt naar overeenkomende Clans en past de lijst aan.',
        'De applicatie toont de vernieuwde lijst aan de gebruiker.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-09 is uitgevoerd.',
      postcondition: 'De overeenkomende clans zijn aan de gebruiker getoond.'
    },
    {
      id: 'UC-11',
      name: 'Detailscherm Clan openen',
      description: 'Een gebruiker kan de details van een clan opvragen.',
      scenario: ['De gebruiker klikt op een clan.', 'De applicatie toont het detailscherm Clan.'],
      actor: 'Gebruiker',
      precondition: 'UC-09 is uitgevoerd.',
      postcondition: 'De details van een clan zijn aan de gebruiker getoond.'
    },
    {
      id: 'UC-12',
      name: 'Clan toevoegen',
      description: 'Een gebruiker kan een clan aanmaken en opslaan.',
      scenario: [
        'De gebruiker klikt op de Nieuwe Clan knop.',
        'De applicatie opent het Nieuwe Clan formulier',
        'De gebruiker vult de gegevens in en klikt op opslaan',
        'De applicatie slaat de clan op in de database.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-09 is uitgevoerd',
      postcondition: 'Er is een nieuwe clan opgeslagen in de database'
    },
    {
      id: 'UC-13',
      name: 'Clan bewerken',
      description: 'Een gebruiker kan een clan bewerken en opslaan.',
      scenario: [
        'De gebruiker klikt op de Bewerken knop.',
        'De applicatie opent het Clan Bewerken formulier',
        'De gebruiker past de gegevens aan en klikt op opslaan',
        'De applicatie update de clan in de database.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-11 is uitgevoerd',
      postcondition: 'De gegevens van de clan zijn bewerkt en opgeslagen in de database'
    },
    {
      id: 'UC-14',
      name: 'Clan verwijderen',
      description: 'Een gebruiker kan een clan verwijderen.',
      scenario: [
        'De gebruiker klikt op de Verwijder knop.',
        'De applicatie verwijdert de clan uit de database.',
        'De applicatie toont de vernieuwde lijst aan de gebruiker.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-11 is uitgevoerd',
      postcondition: 'De clan is verwijderd uit de database'
    },
    {
      id: 'UC-15',
      name: 'Gebruiker toevoegen aan een clan',
      description: 'Een gebruiker kan een speler aan een clan toevoegen.',
      scenario: [
        'De gebruiker klikt op de Voeg Speler Toe knop.',
        'De applicatie opent een keuzescherm met spelers.',
        'De gebruiker selecteert een speler en klikt op opslaan.',
        'De applicatie voegt de speler toe aan de clan.',
        'De applicatie toont de vernieuwde lijst aan de gebruiker.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-11 is uitgevoerd',
      postcondition: 'De speler is toegevoegd aan de clan'
    },
    {
      id: 'UC-16',
      name: 'Gebruiker verwijderen uit een clan',
      description: 'Een gebruiker kan een speler uit een clan verwijderen.',
      scenario: [
        'De gebruiker klikt in de lijst op de Verwijder Speler knop.',
        'De applicatie verwijdert de speler uit de clan.',
        'De applicatie toont de vernieuwde lijst aan de gebruiker.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-11 is uitgevoerd',
      postcondition: 'De speler is verwijderd uit de clan'
    },
    {
      id: 'UC-17',
      name: 'Bericht plaatsen bij een clan',
      description: 'Een gebruiker kan een bericht plaatsen bij een clan.',
      scenario: [
        'De gebruiker typt een bericht in het inputveld.',
        'De applicatie toont de input zodra de gebruiker iets typt.',
        'De gebruiker klikt op de Verzenden knop.',
        'De applicatie slaat het bericht op in de database.',
        'De applicatie toont de vernieuwde lijst met berichten aan de gebruiker.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-11 is uitgevoerd',
      postcondition: 'Er is een bericht geplaatst op de pagina van de clan.'
    },
    {
      id: 'UC-18',
      name: 'Base toevoegen aan een speler',
      description: 'Een gebruiker kan een base toevoegen aan een speler.',
      scenario: [
        'De gebruiker klikt op de Base Toevoegen knop.',
        'De applicatie opent het Nieuwe Base formulier.',
        'De gebruiker vult de gegevens in en klikt op de Verzenden knop.',
        'De applicatie voegt de base toe aan de speler.',
        'De applicatie toont de vernieuwde lijst met bases aan de gebruiker.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-05 is uitgevoerd',
      postcondition: 'De base is toegevoegd aan de speler.'
    },
    {
      id: 'UC-19',
      name: 'Base verwijderen van een speler',
      description: 'Een gebruiker kan een base verwijderen van een speler.',
      scenario: [
        'De gebruiker klikt op de Base Verwijderen knop.',
        'De applicatie verwijdert de base van de speler.',
        'De applicatie toont de vernieuwde lijst met bases aan de gebruiker.'
      ],
      actor: 'Gebruiker',
      precondition: 'UC-05 is uitgevoerd',
      postcondition: 'De base is verwijderd van de speler.'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
