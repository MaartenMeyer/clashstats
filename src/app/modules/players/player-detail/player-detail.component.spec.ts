import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { PlayerDetailComponent } from './player-detail.component'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { PlayerService } from '@app/core/services/player.service'

import { NO_ERRORS_SCHEMA } from '@angular/core'
import { AuthenticationService } from '@app/core/services/authentication.service'
import { Player } from '@app/core/models/player.model'
import { User } from '@app/core/models/user.model'

describe('PlayerDetailComponent', () => {
  let component: PlayerDetailComponent
  let fixture: ComponentFixture<PlayerDetailComponent>
  const user: User = {
    _id: 'user-1',
    username: 'test',
    email: 'test@email.com',
    password: 'Test12345',
    token: 'token',
    deserialize: null
  }
  const player: Player = {
    playerId: 'player-1',
    name: 'Player 1',
    level: 100,
    creator: user,
    clan: null,
    bases: null,
    image: 'image',
    deserialize: null
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [PlayerDetailComponent],
      providers: [AuthenticationService, PlayerService],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerDetailComponent)
    component = fixture.componentInstance
    component.currentUser = user
    component.player = player
    fixture.detectChanges()
  })

  afterEach(() => {
    TestBed.resetTestingModule()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should render player name', () => {
    const compiled = fixture.debugElement.nativeElement
    expect(compiled.querySelector('h3').textContent).toContain(player.name)
  })
})
