import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { first } from 'rxjs/operators'

import { PlayerService } from '@app/core/services/player.service'
import { AuthenticationService } from '@app/core/services/authentication.service'
import { Player } from '@app/core/models/player.model'
import { User } from '@app/core/models/user.model'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { PlayerUpdateComponent } from '../modals/player-update/player-update.component'

@Component({
  selector: 'app-player-detail',
  templateUrl: './player-detail.component.html',
  styleUrls: ['./player-detail.component.scss']
})
export class PlayerDetailComponent implements OnInit {
  player: Player
  currentUser: User
  showError = false
  error = ''

  constructor(
    private playerService: PlayerService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal
  ) {
    this.currentUser = this.authenticationService.currentUserValue
  }

  ngOnInit() {
    this.loadPlayer()
  }

  private reloadCurrentRoute() {
    const currentUrl = this.router.url
    this.router.navigate([currentUrl])
  }

  private loadPlayer() {
    this.route.data.subscribe(data => {
      if (data.player.error) {
        this.showError = true
        this.error = data.player.error
      } else {
        this.showError = false
        this.player = data.player
      }
    })
  }

  deletePlayer() {
    if (confirm(`Are you sure you want to delete this player?`)) {
      this.playerService
        .deletePlayerById(this.player.playerId)
        .pipe(first())
        .subscribe(
          response => {
            this.reloadCurrentRoute()
          },
          error => {
            this.error = error
            this.showError = true
            window.scrollTo(0, 0)
          }
        )
    }
  }

  openUpdateModal() {
    const modalRef = this.modalService.open(PlayerUpdateComponent)
    modalRef.componentInstance.player = this.player
    modalRef.componentInstance.currentUser = this.currentUser
    modalRef.result
      .then(result => {
        this.reloadCurrentRoute()
      })
      .catch(error => {
        this.error = error
        this.showError = true
        window.scrollTo(0, 0)
      })
  }
}
