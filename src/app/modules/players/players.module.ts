import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ScrollingModule } from '@angular/cdk/scrolling'

import { PlayersRoutingModule } from './players-routing.module'
import { PlayersComponent } from './players.component'
import { PlayerDetailComponent } from './player-detail/player-detail.component'
import { PlayersListItemComponent } from './players-list-item/players-list-item.component'
import { PlayerCreateComponent } from './modals/player-create/player-create.component'
import { PipesModule } from '../pipes/pipes.module'
import { PlayerUpdateComponent } from './modals/player-update/player-update.component'
import { PlayerBasesComponent } from './player-bases/player-bases.component'
import { BaseCreateComponent } from './modals/base-create/base-create.component'
import { PlayersOverviewComponent } from './players-overview/players-overview.component'

@NgModule({
  imports: [
    PlayersRoutingModule,
    CommonModule,
    ScrollingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule
  ],
  declarations: [
    PlayersComponent,
    PlayerDetailComponent,
    PlayersListItemComponent,
    PlayerCreateComponent,
    PlayerUpdateComponent,
    PlayerBasesComponent,
    BaseCreateComponent,
    PlayersOverviewComponent
  ],
  entryComponents: [PlayerCreateComponent, PlayerUpdateComponent, BaseCreateComponent]
})
export class PlayersModule {}
