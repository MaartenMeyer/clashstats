import { Component, OnInit } from '@angular/core'
import { PlayerService } from '@app/core/services/player.service'
import { Router, ActivatedRoute } from '@angular/router'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { BaseCreateComponent } from '../modals/base-create/base-create.component'
import { Player } from '@app/core/models/player.model'
import { User } from '@app/core/models/user.model'
import { AuthenticationService } from '@app/core/services/authentication.service'
import { first } from 'rxjs/operators'

@Component({
  selector: 'app-player-bases',
  templateUrl: './player-bases.component.html',
  styleUrls: ['./player-bases.component.scss']
})
export class PlayerBasesComponent implements OnInit {
  player: Player
  currentUser: User
  showError = false
  error = ''

  constructor(
    private playerService: PlayerService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal
  ) {
    this.currentUser = this.authenticationService.currentUserValue
  }

  ngOnInit() {
    this.loadPlayer()
  }

  private reloadCurrentRoute() {
    const currentUrl = this.router.url
    this.router.navigateByUrl(`/players/${this.player.playerId}`, { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl])
    })
  }

  private loadPlayer() {
    this.route.data.subscribe(data => {
      if (data.player.error) {
        this.showError = true
        this.error = data.player.error
      } else {
        this.showError = false
        this.player = data.player
      }
    })
  }

  removeBase(id) {
    if (confirm(`Delete this base?`)) {
      this.playerService
        .deleteBaseById(this.player.playerId, id)
        .pipe(first())
        .subscribe(
          response => {
            this.reloadCurrentRoute()
          },
          error => {
            this.error = error
            this.showError = true
            window.scrollTo(0, 0)
          }
        )
    }
  }

  openModal() {
    const modalRef = this.modalService.open(BaseCreateComponent)
    modalRef.componentInstance.player = this.player
    modalRef.result
      .then(result => {
        this.reloadCurrentRoute()
      })
      .catch(error => {})
  }
}
