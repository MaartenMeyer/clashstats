import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { PlayerBasesComponent } from './player-bases.component'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { PlayerService } from '@app/core/services/player.service'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { AuthenticationService } from '@app/core/services/authentication.service'

describe('PlayerBasesComponent', () => {
  let component: PlayerBasesComponent
  let fixture: ComponentFixture<PlayerBasesComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [PlayerBasesComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [PlayerService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerBasesComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  afterEach(() => {
    TestBed.resetTestingModule()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
