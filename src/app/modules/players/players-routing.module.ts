import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { AuthGuard } from '@app/core/guards/auth.guard'

import { PlayersComponent } from './players.component'
import { PlayerDetailComponent } from './player-detail/player-detail.component'
import { PlayerResolver } from '@app/core/services/resolvers/player.resolver'
import { PlayerBasesComponent } from './player-bases/player-bases.component'
import { PlayersOverviewComponent } from './players-overview/players-overview.component'

const playersRoutes: Routes = [
  {
    path: '',
    component: PlayersComponent,
    children: [
      {
        path: '',
        redirectTo: 'overview',
        pathMatch: 'full'
      },
      {
        path: 'overview',
        component: PlayersOverviewComponent
      },
      {
        path: ':id',
        children: [
          {
            path: '',
            redirectTo: 'details',
            pathMatch: 'full'
          },
          {
            path: 'details',
            component: PlayerDetailComponent,
            resolve: { player: PlayerResolver },
            runGuardsAndResolvers: 'always'
          },
          {
            path: 'bases',
            component: PlayerBasesComponent,
            resolve: { player: PlayerResolver },
            runGuardsAndResolvers: 'always'
          }
        ]
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(playersRoutes)],
  exports: [RouterModule],
  providers: [PlayerResolver]
})
export class PlayersRoutingModule {}
