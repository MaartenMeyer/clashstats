import { Component, OnInit, Input } from '@angular/core'
import { Player } from '@app/core/models/player.model'

@Component({
  selector: 'app-players-list-item',
  templateUrl: './players-list-item.component.html',
  styleUrls: ['./players-list-item.component.scss']
})
export class PlayersListItemComponent implements OnInit {
  @Input() player: Player

  constructor() {}

  ngOnInit() {}
}
