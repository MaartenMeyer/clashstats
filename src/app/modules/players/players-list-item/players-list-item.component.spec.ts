import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { PlayersListItemComponent } from './players-list-item.component'
import { RouterTestingModule } from '@angular/router/testing'

describe('PlayersListItemsComponent', () => {
  let component: PlayersListItemComponent
  let fixture: ComponentFixture<PlayersListItemComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlayersListItemComponent],
      imports: [RouterTestingModule]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayersListItemComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  afterEach(() => {
    TestBed.resetTestingModule()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
