import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { BaseCreateComponent } from './base-create.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { PlayerService } from '@app/core/services/player.service'

describe('BaseCreateComponent', () => {
  let component: BaseCreateComponent
  let fixture: ComponentFixture<BaseCreateComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, HttpClientTestingModule],
      declarations: [BaseCreateComponent],
      providers: [NgbActiveModal, PlayerService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseCreateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
