import { Component, OnInit, Input } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PlayerService } from '@app/core/services/player.service'
import { Player } from '@app/core/models/player.model'
import { first } from 'rxjs/operators'

@Component({
  selector: 'app-base-create',
  templateUrl: './base-create.component.html',
  styleUrls: ['./base-create.component.scss']
})
export class BaseCreateComponent implements OnInit {
  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private playerService: PlayerService
  ) {}

  @Input() player: Player
  addBaseForm: FormGroup
  showError = false
  error: ''
  submitted = false
  fileData: File = null
  previewUrl: any = null
  uploadedFilePath: string = null

  ngOnInit() {
    this.addBaseForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      link: [
        null,
        [
          Validators.required,
          Validators.pattern(/^(http|https):\/\/link[.]clashofclans[.]com\/[a-z]{2}\?action=OpenLayout&id=TH/)
        ]
      ],
      image: [null, [Validators.required]]
    })
  }

  get form() {
    return this.addBaseForm.controls
  }

  readUrl(fileInput: any) {
    this.fileData = fileInput.target.files[0] as File
    this.preview()
  }

  preview() {
    const mimeType = this.fileData.type
    if (mimeType.match(/image\/*/) == null) {
      return
    }

    const reader = new FileReader()
    reader.readAsDataURL(this.fileData)
    reader.onload = event => {
      this.previewUrl = reader.result
    }
  }

  onSubmit() {
    this.submitted = true
    if (this.addBaseForm.invalid) {
      return
    }

    this.playerService
      .addBaseToPlayer(
        this.player.playerId,
        this.addBaseForm.value.title,
        this.addBaseForm.value.link,
        this.fileData
      )
      .pipe(first())
      .subscribe(
        data => {
          this.activeModal.close('Base added')
        },
        error => {
          this.showError = true
          this.error = error
        }
      )
  }
}
