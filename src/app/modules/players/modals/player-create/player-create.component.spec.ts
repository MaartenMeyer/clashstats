import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { PlayerCreateComponent } from './player-create.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { PlayerService } from '@app/core/services/player.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('PlayerCreateComponent', () => {
  let component: PlayerCreateComponent
  let fixture: ComponentFixture<PlayerCreateComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, HttpClientTestingModule],
      declarations: [PlayerCreateComponent],
      providers: [NgbActiveModal, PlayerService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerCreateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
