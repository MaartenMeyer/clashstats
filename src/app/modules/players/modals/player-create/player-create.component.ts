import { Component, OnInit, Input } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { first } from 'rxjs/operators'

import { PlayerService } from '@app/core/services/player.service'

@Component({
  selector: 'app-player-create',
  templateUrl: './player-create.component.html',
  styleUrls: ['./player-create.component.scss']
})
export class PlayerCreateComponent implements OnInit {
  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private playerService: PlayerService
  ) {}

  @Input() id: number
  createPlayerForm: FormGroup
  showError = false
  error: ''
  submitted = false
  fileData: File = null
  previewUrl: any = null
  uploadedFilePath: string = null

  ngOnInit() {
    this.createPlayerForm = this.formBuilder.group({
      playerId: ['', [Validators.required]],
      name: ['', [Validators.required]],
      level: [null, [Validators.required]],
      image: [null, [Validators.required]]
    })
  }

  get form() {
    return this.createPlayerForm.controls
  }

  readUrl(fileInput: any) {
    this.fileData = fileInput.target.files[0] as File
    this.preview()
  }

  preview() {
    const mimeType = this.fileData.type
    if (mimeType.match(/image\/*/) == null) {
      return
    }

    const reader = new FileReader()
    reader.readAsDataURL(this.fileData)
    reader.onload = event => {
      this.previewUrl = reader.result
    }
  }

  onSubmit() {
    this.submitted = true
    if (this.createPlayerForm.invalid) {
      return
    }

    this.playerService
      .createPlayer(
        this.createPlayerForm.value.playerId,
        this.createPlayerForm.value.name,
        this.createPlayerForm.value.level,
        this.fileData
      )
      .pipe(first())
      .subscribe(
        data => {
          this.activeModal.close('Player created')
        },
        error => {
          this.showError = true
          this.error = error
        }
      )
  }
}
