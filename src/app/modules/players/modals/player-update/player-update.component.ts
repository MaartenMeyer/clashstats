import { Component, OnInit, Input } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Player } from '@app/core/models/player.model'
import { User } from '@app/core/models/user.model'
import { first } from 'rxjs/operators'
import { PlayerService } from '@app/core/services/player.service'

@Component({
  selector: 'app-player-update',
  templateUrl: './player-update.component.html',
  styleUrls: ['./player-update.component.scss']
})
export class PlayerUpdateComponent implements OnInit {
  constructor(
    public activeModal: NgbActiveModal,
    public formBuilder: FormBuilder,
    private playerService: PlayerService
  ) {}

  @Input() player: Player
  @Input() currentUser: User
  updatePlayerForm: FormGroup
  showError = false
  error: ''
  submitted = false

  ngOnInit() {
    this.updatePlayerForm = this.formBuilder.group({
      playerId: [this.player.playerId, [Validators.required]],
      name: [this.player.name, [Validators.required]],
      level: [this.player.level, [Validators.required]]
    })
  }

  get form() {
    return this.updatePlayerForm.controls
  }

  onSubmit() {
    this.submitted = true
    if (this.updatePlayerForm.invalid) {
      return
    }

    this.playerService
      .updatePlayerById(
        this.updatePlayerForm.value.playerId,
        this.updatePlayerForm.value.name,
        this.updatePlayerForm.value.level
      )
      .pipe(first())
      .subscribe(
        data => {
          this.activeModal.close('Player updated')
        },
        error => {
          this.showError = true
          this.error = error
        }
      )
  }
}
