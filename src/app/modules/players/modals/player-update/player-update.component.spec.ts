import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { PlayerUpdateComponent } from './player-update.component'
import { FormsModule, ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { PlayerService } from '@app/core/services/player.service'
import { User } from '@app/core/models/user.model'
import { Player } from '@app/core/models/player.model'

describe('PlayerUpdateComponent', () => {
  let component: PlayerUpdateComponent
  let fixture: ComponentFixture<PlayerUpdateComponent>
  const formBuilder: FormBuilder = new FormBuilder()
  const user: User = {
    _id: 'user-1',
    username: 'test',
    email: 'test@email.com',
    password: 'Test12345',
    token: 'token',
    deserialize: null
  }
  const player: Player = {
    playerId: 'player-1',
    name: 'Player 1',
    level: 100,
    creator: user,
    clan: null,
    bases: null,
    image: 'image',
    deserialize: null
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, HttpClientTestingModule],
      declarations: [PlayerUpdateComponent],
      providers: [NgbActiveModal, PlayerService, { provide: FormBuilder, useValue: formBuilder }]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerUpdateComponent)
    component = fixture.componentInstance
    component.currentUser = user
    component.player = player
    component.updatePlayerForm = formBuilder.group({
      playerId: [player.playerId, [Validators.required]],
      name: [player.name, [Validators.required]],
      level: [player.level, [Validators.required]]
    })
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
