import { Component, OnInit, Input } from '@angular/core'
import { Router } from '@angular/router'
import { first } from 'rxjs/operators'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'

import { PlayerCreateComponent } from '../modals/player-create/player-create.component'
import { PlayerService } from '@app/core/services/player.service'
import { AuthenticationService } from '@app/core/services/authentication.service'
import { Player } from '@app/core/models/player.model'
import { User } from '@app/core/models/user.model'

@Component({
  selector: 'app-players-overview',
  templateUrl: './players-overview.component.html',
  styleUrls: ['./players-overview.component.scss']
})
export class PlayersOverviewComponent implements OnInit {
  @Input()
  players: Player[] = []
  currentUser: User
  searchText = ''
  showError = false
  error = ''
  loading = false

  constructor(
    private playerService: PlayerService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.currentUser = this.authenticationService.currentUserValue
  }

  ngOnInit() {
    this.loading = true
    this.loadAllPlayers()
  }

  private reloadCurrentRoute() {
    const currentUrl = this.router.url
    this.router.navigateByUrl('../', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl])
    })
  }

  private loadAllPlayers() {
    this.playerService
      .getAllPlayers()
      .pipe(first())
      .subscribe(players => {
        this.loading = false
        this.players = players
      })
  }

  openModal() {
    const modalRef = this.modalService.open(PlayerCreateComponent)
    modalRef.componentInstance.id = 10
    modalRef.result
      .then(result => {
        this.reloadCurrentRoute()
      })
      .catch(error => {})
  }
}
