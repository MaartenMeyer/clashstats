import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { PlayersOverviewComponent } from './players-overview.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { PlayerDetailComponent } from '../player-detail/player-detail.component'
import { PlayersListItemComponent } from '../players-list-item/players-list-item.component'
import { ScrollingModule } from '@angular/cdk/scrolling'
import { PipesModule } from '@app/modules/pipes/pipes.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

describe('PlayersComponent', () => {
  let component: PlayersOverviewComponent
  let fixture: ComponentFixture<PlayersOverviewComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        ScrollingModule,
        PipesModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [PlayersOverviewComponent, PlayerDetailComponent, PlayersListItemComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayersOverviewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
