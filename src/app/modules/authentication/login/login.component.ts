import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { first } from 'rxjs/operators'
import { ActivatedRoute, Router } from '@angular/router'

import { AuthenticationService } from '@app/core/services/authentication.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  error = ''
  submitted = false
  returnUrl: string

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/'])
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })

    const s = 'returnUrl'
    this.returnUrl = this.route.snapshot.queryParams[s] || '/'
  }

  get form() {
    return this.loginForm.controls
  }

  onSubmit() {
    this.submitted = true
    if (this.loginForm.invalid) {
      return
    }

    this.authenticationService
      .login(this.form.email.value, this.form.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/' + this.returnUrl])
        },
        error => {
          this.error = error
        }
      )
  }
}
