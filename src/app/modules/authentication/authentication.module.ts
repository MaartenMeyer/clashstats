import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AuthenticationRoutingModule } from './authentication-routing'
import { LoginComponent } from './login/login.component'
import { RegisterComponent } from './register/register.component'

@NgModule({
  imports: [CommonModule, AuthenticationRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [LoginComponent, RegisterComponent]
})
export class AuthenticationModule {}
