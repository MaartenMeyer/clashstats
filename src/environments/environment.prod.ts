export const environment = {
  production: true,
  baseUrl: 'https://clashstats-api.herokuapp.com/api'
}
